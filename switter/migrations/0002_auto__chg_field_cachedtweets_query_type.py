# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'CachedTweets.query_type'
        db.alter_column('switter_cachedtweets', 'query_type', self.gf('django.db.models.fields.CharField')(max_length=20))

    def backwards(self, orm):

        # Changing field 'CachedTweets.query_type'
        db.alter_column('switter_cachedtweets', 'query_type', self.gf('django.db.models.fields.CharField')(max_length=255))

    models = {
        'switter.cachedtweets': {
            'Meta': {'unique_together': "(('query_type', 'query_value'),)", 'object_name': 'CachedTweets'},
            'cached_response': ('django.db.models.fields.TextField', [], {'default': "'{}'", 'blank': 'True'}),
            'created': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'modified': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now', 'blank': 'True'}),
            'query_type': ('django.db.models.fields.CharField', [], {'default': "'user_timeline'", 'max_length': '20'}),
            'query_value': ('django.db.models.fields.CharField', [], {'default': "''", 'max_length': '255', 'blank': 'True'})
        }
    }

    complete_apps = ['switter']