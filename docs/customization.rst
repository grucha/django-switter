Customization & others
======================

* Fine tune Switter plugin by overriding ``cms/plugins/switter/default.html`` template ::

    {# cms/plugins/switter/default.html #}
    {% load sekizai_tags %}

    <section class="switter" id="switter-{{ object.id }}">
        {% if object.header %}
            <h2>{{ object.header }}</h2>
        {% endif %}

        <ul class="tweets">{# loading... #}</ul>
    </section>

    {% addtoblock "js" %}
        <!-- Assuming you have query already loaded. If not, please do include it. -->
        <script src="{{ STATIC_URL }}js/jquery.switter.js"></script>
        <script type="text/javascript">
            $(function(){
                $('#switter-{{ object.id }} .tweets').switter({
                    // these 3 are Switter-specific:
                    url: '{% url switter_tweets query_type=object.query_type %}',
                    url_params: {{ object.get_jquery_ajax_data|safe }},
                    preloaded_tweets: {{ object.get_tweets_json|safe }},

                    // put the rest of your usual jquery.tweet.js configuration here...
                    count: {{ object.count }},
                    loading_text: "Oooh, loading tweets...",

                    // for example fancy tweet template: 
                    template: '<p>{text}</p><a class="tweet_time" href="{tweet_url}">{time}</a> <a class="tweet_user" href="{user_url}">by @{screen_name}</a>'
                });
            });
        </script>
    {% endaddtoblock %}

* Add some styles to your tweets


Usage without django-cms
------------------------

* Not using django-cms? Not a problem! Just use modified Switter plugin template code and include it in your templates ::

    {# my/fancy/switter/_tweets.html to be included here and there #}
    <section id="switter">
        <h2>Our tweets</h2>
        <ul class="tweets"></ul>
    </section>

    {# you probably want to paste this somewhere at the bottom of your base.html #}
    <script src="{{ STATIC_URL }}js/jquery.switter.js"></script>
    <script type="text/javascript">
        // add this javascript at the bottom of your base.html template
        $(function(){
            // get user timeline...
            var switter_url = '{% url switter_tweets query_type='user_timeline' %}'
            var switter_url_params = {
                screen_name: 'verybritishproblems',
                count: 5,
                exclude_replies: false, // optional
                include_rts: true // optional
            }

            // or any Twitter search results:
            var switter_url = '{% url switter_tweets query_type='search' %}'
            var switter_url_params = {
                q: 'from:verybritishproblems', // twitter search query
                count: 5
            }

            $('#switter .tweets').switter({
                // these 3 are Switter-specific:
                url: switter_url, 
                url_params: switter_url_params,
                preloaded_tweets: {{ my_cached_tweets_json|safe }}, // optional (will ajax for tweets if not present)

                // put the rest of your usual jquery.tweet.js configuration here...
                count: 5
            });
        });
    </script>

